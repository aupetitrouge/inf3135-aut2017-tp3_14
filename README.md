# Travail pratique 3

## Description

Ce programme est un jeu de platformes en 2D dont l'objectif est de récupérer tous les beignes se trouvant dans le tableau.

Il est réalisé dans le cadre du cours INF3135 - Construction et maintenance de logiciels de la session d'automne 2017 à l'UQAM.

## Auteurs
- Alexandre Côté Cyr - COTA07049302
- Koffi Ballo - BALK13089406
- Éric Balthazard - BALE02059505

Groupe: 020

## Compilation
Une fois les [dépendances](#dépendances) installées, il suffit de lancer la commande `make` pour compiler le projet.
L'exécutable généré sera dans le sous-dossier `bin/` sous le nom de `tp3`.

## Fonctionnement
Pour démarrer le jeu, il faut lancer l'exécutable `tp3` à partir du dossier `bin/`. Cela est très important puisque les assets sont recueillis à partir de chemin relatif de se dossier. Une fenêtre avec le menu du jeu sera alors affichée. Il faut appuyer sur la touche `1` pour démarrer le jeu ou `2` pour quitter.

Lors du jeu lui-même, le personnage est contrôlé par les flèches directionnelles du clavier. L'objectif est de récolter tous les beignes du tableau avant que le temps affiché dans le coin de l'écran ne soit écoulé. Une fois la partie terminée, un message de victoire ou de défaite s'affichera selon l'issue de la partie. Il suffit ensuite d'appuyer sur une touche pour retourner  au menu.

Il est possible de quitter le jeu à tout moment en appuyant sur la touche `échap` ou en appuyant sur le bouton `X` dans le coin de la fenêtre.

## Contribuer
Les lignes directrices pour contribuer au projet se trouvent dans le fichier [CONTRIBUTING](CONTRIBUTING.md).

## Tâches
- [x] Créer le Makefile. (Alexandre)
- [x] Créer le fichier d'intégration continue pour vérifier que le projet compile. (Alexandre)
- [x] Intégrer les fonctionnalités existantes de Maze-SDL. (Alexandre)
- [x] Gérer les déplacements du personnage. (Alexandre)
- [X] Créer l'image du tableau de jeu. (Éric)
- [X] Gérer les coordonnées des murs du tableau. (Éric)
- [X] Créer les images pour le menu. (Éric)
- [X] Afficher les beignes. (Koffi)
- [X] Gérer les coordonnées des beignes. (Koffi)
- [X] Gérer les collisions entre les objets. (Alexandre)(Eric)(Koffi)
- [X] Gérer les conditions de victoire. (Koffi)
- [X] Afficher les écrans de victoire (Koffi)
- [X] Gérer les conditions de de défaite. (Alexandre)
- [X] Afficher l'écran de de défaite (Alexandre)

## Plateformes supportées
- Archlinux avec Noyau linux version 4.14.4
- Ubuntu Linux
- Linux Mint 18.3

## Dépendances
- [SDL2](https://www.libsdl.org/), une bibliothèque permettant d'interagir avec les composants multimedias d'un ordinateur.
- [SDL2_image](https://www.libsdl.org/projects/SDL_image/), une bibliothèque permettant l'utilisation d'images en textures et surfaces SDL.
- [SDL2_TTF](https://www.libsdl.org/projects/SDL_ttf/), une bibliothèque permettant l'utilisation de polices de caractères en textures et surfaces SDL.
- [TMX](https://github.com/baylej/tmx), une bibliothèque permettant de charger des cartes carrelées pour les jeux.

## Références
- [Maze-SDL](https://bitbucket.org/ablondin-projects/maze-sdl), utilisé comme base pour ce projet.
- [Tuto SDL - Mouvement](http://loka.developpez.com/tutoriel/sdl/mouvements/), pour les mouvements utilisant la vélocité et pour les collisions.
- [C++ SDL 2D Hopping (gravity)](https://stackoverflow.com/questions/14426870/c-sdl-2d-hopping-gravity), pour la simulation de la gravité.
